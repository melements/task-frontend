module.exports = {
  stripPrefix: 'build/',
  staticFileGlobs: [
    'build/*.png',
    'build/index.html',
    'build/manifest.json',
    'build/static/**/!(*map*)',
  ],
  dontCacheBustUrlsMatching: /\.\w{8}\./,
  navigateFallback: 'index.html',
  runtimeCaching: [{
    urlPattern: /https?:\/\/fonts.+/,
    handler: 'fastest',
  }, {
    urlPattern: /https?:\/\/images.+/,
    handler: 'fastest',
  }],
};
