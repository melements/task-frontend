This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode with test coverage.
Launches three types of tests. Action / Reducer tests made with Jes

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

### `npm run build:docs`

Builds documentation available in `build/docs` folder.


## Technology used
- React - https://reactjs.org/
- Create React App - https://github.com/facebook/create-react-app
- Redux - https://github.com/reduxjs/redux
- Redux Thunk - https://github.com/reduxjs/redux-thunk
- React Router v4 - https://github.com/ReactTraining/react-router
- Lodash - https://github.com/lodash/lodash
- Immutability Helper - https://github.com/kolodny/immutability-helper
- React Masonry - https://github.com/eiriklv/react-masonry-component

## Testing technologies used
- Jest - https://github.com/facebook/jest
- Enzyme - https://github.com/airbnb/enzyme
- Puppeteer - https://github.com/GoogleChrome/puppeteer