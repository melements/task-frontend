import { getLanguageList } from './github';
import Api from './api';

describe('testing api', () => {
  beforeEach(() => {
    fetch.resetMocks();
  });

  it('should call getLanguageList and return data for given params', async () => {
    fetch.mockResponseOnce(
      JSON.stringify({
        Shell: 3456,
        Elixir: 67,
      }),
    );

    const result = await getLanguageList('shieldfy', 'API-Security-Checklist');
    expect(result).toEqual({ Shell: '98%', Elixir: '2%' });
  });

  it('returns an error when GET request fails', async () => {
    fetch.mockReject(new Error('An internal error has occured'));
    try {
      await Api.get('www.example.com');
    } catch (error) {
      expect(error.message).toBe('An internal error has occured');
    }
  });

  it('returns an 401 when GET fails', async () => {
    fetch.mockResponseOnce(JSON.stringify({}), { status: 401 });
    try {
      await Api.get('http://example.com');
    } catch (error) {
      expect(error).toBeUndefined();
    }
  });

  it('returns an 204 when GET fails', async () => {
    fetch.mockResponseOnce(JSON.stringify({}), { status: 204 });
    try {
      const response = await Api.get('http://example.com');
      expect(response).toBeUndefined();
    } catch (error) {
      expect(error).toBeUndefined();
    }
  });

  it('returns an 422 when GET fails', async () => {
    fetch.mockResponseOnce(JSON.stringify({}), { status: 422 });
    try {
      await Api.get('http://example.com');
    } catch (error) {
      expect(error.message).toBe('An internal error has occured');
    }
  });

  it('returns an 425 when GET fails', async () => {
    fetch.mockResponseOnce(JSON.stringify({}), { status: 425 });
    try {
      await Api.get('http://example.com');
    } catch (error) {
      expect(error).toBeDefined();
    }
  });
});
