/**
 * @description get favourites from storage
 * @returns results from storage
 * @param {String} value
 */
const getValue = (value) => {
  const item = window.localStorage.getItem(value);
  const parsedValue = JSON.parse(item);

  return parsedValue;
};

/**
 * @description set value in storage
 * @param {String} key
 * @param value
 */
const setValue = (key, value) => window.localStorage.setItem(key, JSON.stringify(value));

export {
  getValue,
  setValue,
};
