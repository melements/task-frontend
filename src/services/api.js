import { BASE_URL } from 'modules/app/config/config';

/**
 * @description Api service to handle requests
 */
class Api {
  constructor() {
    this.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
    this.baseUrl = BASE_URL;
  }

  /**
   * @description get route and params and
   * @returns new route
   * @param {String} route
   * @param {Object} params
   */
  static getRouteWithParams(route, params) {
    const tmp = Object.keys(params).map(key => `${key}=${params[key]}`);
    return `${route}?${tmp.join('&')}`;
  }

  /**
   * @description gets route, params and
   * @returns xhr request
   * @param {String} route
   * @param {Object} params
   * @param {String} url
   */
  get(route, params, url) {
    let newRoute = route;
    if (params) {
      newRoute = Api.getRouteWithParams(route, params);
    }
    return this.xhr(newRoute, null, 'GET', url);
  }

  /**
   * @description XMLHttpRequest request
   * @async
   * @param {String} route
   * @param {Object} params
   * @param {String} method
   * @param {String} uri
   */
  async xhr(route, params, method, uri = this.baseUrl) {
    const url = `${uri}${route}`;
    const options = {
      method,
      url,
      headers: this.headers,
    };

    return fetch(url, options)
      .then((response) => {
        switch (response.status) {
          case 200:
          case 201:
            return response.json();
          case 204:
            return Promise.resolve();
          case 422: {
            return response.json().then((data) => {
              const message = data.message || 'An internal error has occured';
              return Promise.reject(new Error(message));
            });
          }
          case 401:
            return Promise.reject();
          default: {
            const message = response.statusText || 'An internal error has occured';
            return Promise.reject(new Error(message));
          }
        }
      })
      .catch(err => Promise.reject(err));
  }
}

export default new Api();
