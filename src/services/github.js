import _set from 'lodash/set';
import _isEmpty from 'lodash/isEmpty';
import Api from './api';

/**
 * @description Helper method to get query
 * @param {Object} params
 */
const getQuery = (params) => {
  const {
    name,
    language,
    pageSize,
    page,
  } = params;
  const currentDate = new Date();
  currentDate.setFullYear(currentDate.getFullYear() - 1);
  const lastYear = currentDate.toISOString().slice(0, 10);

  const query = {
    q: `${name}+stars:>10+pushed:>=${lastYear}`,
    language,
    sort: 'stars',
    per_page: pageSize,
    page,
  };

  return query;
};

/**
 * @description get repositories from github
 * @returns repositories response
 * @async
 * @param {Object} params
 */
const getRepositories = async (params) => {
  const requestParams = Object.assign({}, getQuery(params));
  const response = await Api.get('/search/repositories', requestParams);
  return response;
};

/**
 * @description get languages list from github
 * @returns repository's languages list
 * @param {String} owner
 * @param {String} repo
 */
const getLanguageList = async (owner, repo) => {
  const params = Object.assign({}, { owner, repo });
  const response = await Api.get(`/repos/${owner}/${repo}/languages`, params);
  let parsedResponse = {};
  if (!_isEmpty(response)) {
    const totalSum = Object.values(response).reduce((prevItem, item) => prevItem + item);
    parsedResponse = Object.keys(response).reduce((reduced, itemKey) => {
      _set(reduced, [itemKey], `${Math.round((response[itemKey] / totalSum) * 100)}%`);
      return reduced;
    }, {});
  }

  return parsedResponse;
};

export {
  getRepositories,
  getLanguageList,
};
