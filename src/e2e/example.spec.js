const puppeteer = require('puppeteer');

describe('Search Favourites', () => {
  let browser;
  let page;

  beforeAll(async () => {
    browser = await puppeteer.launch({
      headless: true,
    });
    page = await browser.newPage();

    page.emulate({
      viewport: {
        width: 500,
        height: 2400,
      },
      userAgent: '',
    });

    await page.goto('http://localhost:3000/');
  });

  afterAll(() => {
    browser.close();
  });

  test('h2 loads correctly', async () => {
    const h2 = await page.$eval('.title > h2', e => e.innerHTML);
    expect(h2).toBe('Search favourites');
  }, 3000);

  test('Add favourite element correctly', async () => {
    const textValue = 'Hello';
    const languageValue = 'javascript';

    // selectors
    const input = '#textValue';
    const selectBox = '#language';
    const button = '.button-container > button';
    const favourite = '.favourite-item';
    const title = '.word-wrap-break';
    const languageChip = '.chip';

    await page.type(input, textValue);
    await page.select(selectBox, languageValue);
    await page.click(button);

    await page.waitForSelector(`${favourite}`);
    const nodeTitle = await page.$eval(`${favourite} ${title}`, el => el.innerHTML);
    const nodeLanguage = await page.$eval(`${favourite} ${languageChip}`, el => el.innerHTML);

    expect(nodeTitle).toEqual(textValue);
    expect(nodeLanguage).toEqual(languageValue);
  }, 3000);
});
