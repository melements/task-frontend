import React from 'react';
import { shallow } from 'enzyme';
import AddIcon from './add.component';
import LeftArrow from './arrowLeft.component';
import ChevronDown from './chevronDown.component';
import ChevronUp from './chevronUp.component';
import SvgComponent from './trash.component';

describe('shoulds test icon components', () => {
  it('should compare AddIcon with snapshot', () => {
    const wrapper = shallow(<AddIcon />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should compare LeftArrow with snapshot', () => {
    const wrapper = shallow(<LeftArrow />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should compare ChevronDown with snapshot', () => {
    const wrapper = shallow(<ChevronDown />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should compare ChevronUp with snapshot', () => {
    const wrapper = shallow(<ChevronUp />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should compare SvgComponent with snapshot', () => {
    const wrapper = shallow(<SvgComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
