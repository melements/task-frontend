import React from 'react';

/**
 * @description Chevron down icon. Used in Result component to show more details;
 */
const ChevronDown = () => (
  <svg height={30} width={30} viewBox="0 0 48 48">
    <path
      d="M33.17 17.17L24 26.34l-9.17-9.17L12 20l12 12 12-12z"
      fill="#55c4f5"
    />
    <path d="M0 0h48v48H0z" fill="none" />
  </svg>
);

export default ChevronDown;
