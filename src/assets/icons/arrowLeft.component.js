import React from 'react';

/**
 * @description Left arrow icon. Used in navbar to handle back to Home component.
 */
const LeftArrow = () => (
  <svg viewBox="0 0 24 24">
    <path
      fill="none"
      stroke="#000"
      strokeWidth={2}
      strokeMiterlimit={10}
      d="M12.5 21l-9-9 9-9M22 12H3.5"
    />
  </svg>
);

export default LeftArrow;
