import React from 'react';

/**
 * @description Trash component. Used as icon for Delete button in favourite component;
 */
const Trash = () => (
  <svg width={18} height={18}>
    <path d="M17 2h-3.5l-1-1h-5l-1 1H3v2h14zM4 17a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V5H4z" />
  </svg>
);

export default Trash;
