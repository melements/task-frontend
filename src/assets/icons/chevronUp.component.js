import React from 'react';

/**
 * @description Chevron up icon. Used in Result component to show less details;
 */
const ChevronUp = () => (
  <svg height={30} width={30} viewBox="0 0 48 48">
    <path d="M24 16L12 28l2.83 2.83L24 21.66l9.17 9.17L36 28z" fill="#55c4f5" />
    <path d="M0 0h48v48H0z" fill="none" />
  </svg>
);

export default ChevronUp;
