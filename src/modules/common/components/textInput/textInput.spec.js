import React from 'react';
import { shallow } from 'enzyme';
import TextInput from './textInput.component';

describe('shoulds test TextInput component', () => {
  const wrapper = shallow(<TextInput label="label" maxLength={10} name="Name" handleChange={jest.fn()} />);
  const instance = wrapper.instance();

  it('should compare TextInput with snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('simulates onChange', () => {
    const event = {
      target: {
        value: 'testValue',
      },
    };
    instance.onChange(event);
    expect(instance.state.value).toBe('');
  });

  it('check what we get when value lenght is higher than max lenght', () => {
    const event = {
      target: {
        value: '12345678901',
      },
    };
    instance.onChange(event);
    expect(instance.state.value).toBe('');
  });
});
