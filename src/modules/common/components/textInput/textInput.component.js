import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './textInput.scss';

/**
 * @description Custom text input
 */
class TextInput extends PureComponent {
  /**
   * @description state is required here to clear input value after add repository to favourite
   * It also prevent unnecessary renders to other components not related with input
   */
  state = {
    value: '',
  };

  /**
   * @description handles input change and trigger callback to tell if value is selected
   * TextInput value's length cannot be set higher than maxLength
   */
  onChange = (event) => {
    const { target: { value } } = event;
    const { maxLength, handleChange, name } = this.props;

    if (value.length > maxLength) {
      return;
    }

    this.setState({
      value,
    });
    handleChange(name, value);
  }

  /**
   * @description set state from props when successfully added repository to favourite
   * @returns state
   * @param {Boolean} props.isDirty
   */
  static getDerivedStateFromProps(props) {
    if (!props.isDirty) {
      return {
        value: '',
      };
    }
    return null;
  }

  /**
   * @description template for input component. By default empty value is set. After successfull
   * add favourite repository to favourites list, input is set to empty again.
   */
  render() {
    const { value } = this.state;
    const { maxLength, label } = this.props;

    return (
      <form className="input-text-container">
        <label htmlFor="textValue">
          <input
            type="text"
            name="textValue"
            id="textValue"
            value={value}
            onChange={this.onChange}
            className={(value ? 'valid' : null)}
          />
          <span className="label-placeholder">{label}</span>
        </label>
        {maxLength && (
          <span className={`input-counter ${value.length >= maxLength ? 'error' : null}`}>
            {`${value.length}/${maxLength}`}
          </span>
        )}
      </form>
    );
  }
}

/**
 * @description model of properties passed to TextInput component
 */
TextInput.propTypes = {
  maxLength: PropTypes.number,
  handleChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
};

/**
 * @description default properties passed to TextInput component
 */
TextInput.defaultProps = {
  maxLength: 0,
  label: '',
};

export default TextInput;
