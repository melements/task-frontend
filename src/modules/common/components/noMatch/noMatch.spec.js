import React from 'react';
import { shallow } from 'enzyme';
import NoMatch from './noMatch.component';

describe('shoulds test search repo component', () => {
  const wrapper = shallow(<NoMatch />);

  it('should compare no match component with snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
