import React, { PureComponent, Fragment } from 'react';
import './noMatch.scss';

/**
 * Template for handling 404 server error
 * @template 404 Page
 */
class NoMatch extends PureComponent {
  render() {
    return (
      <Fragment>
        <div className="not-found">404 not found</div>
      </Fragment>
    );
  }
}

export default NoMatch;
