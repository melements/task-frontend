import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { getRouteTitle, SEARCH_RESULTS } from 'modules/app/config/config';
import LeftArrow from 'assets/icons/arrowLeft.component';
import logo from 'assets/img/Octocat.png';
import './navbar.scss';

/**
 * @description Navbar component. Displayed in top of view for desktop and mobile devices.
 * @template Navbar
 */
class Navbar extends Component {
  /**
   * go back to Home view
   */
  goBack = () => {
    const { history } = this.props;
    history.push('/');
  }

  /**
   * @description Navbar template for all routes. If SearchResults component
   * is displayed, LeftArrow component is visible at title, to let user go back to
   * Home view
   */
  render() {
    const { children } = this.props;
    const { location: { pathname } } = this.props;
    const navbarTitle = getRouteTitle(pathname);
    return (
      <Fragment>
        <nav className="navbar-component bg-primary shadow">
          <div className="container">
            <div className="title">
              {SEARCH_RESULTS === navbarTitle && (
                <button
                  name="go-back"
                  type="button"
                  className="button"
                  onClick={this.goBack}
                >
                  <LeftArrow />
                </button>
              )}
              <h2>{navbarTitle}</h2>
            </div>
            <img src={logo} alt="logo" className="logo" />
          </div>
        </nav>
        {children}
      </Fragment>
    );
  }
}
/**
 * @description model of properties passed to Navbar component
 */
Navbar.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  history: PropTypes.shape({
    goBack: PropTypes.func,
  }).isRequired,
};
export default withRouter(Navbar);
