import React from 'react';
import { shallow } from 'enzyme';
import Navbar from 'modules/common/components/navbar/navbar.component';

describe('shoulds test Navbar component', () => {
  const location = {
    pathname: '/search',
  };
  const history = {
    goBack: jest.fn(),
  };
  const wrapper = shallow(<Navbar.WrappedComponent location={location} history={history}><div /></Navbar.WrappedComponent>);

  it('should compare Navbar with snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
