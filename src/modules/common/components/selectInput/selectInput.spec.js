import React from 'react';
import { shallow } from 'enzyme';
import SelectInput from 'modules/common/components/selectInput/selectInput.component';

describe('shoulds test SelectInput component', () => {
  const options = {
    javascript: 'JavaScript',
    java: 'Java',
    python: 'Python',
  };
  const wrapper = shallow(<SelectInput options={options} name="Test" handleChange={jest.fn()} />);
  const instance = wrapper.instance();

  it('should compare SelectInput with snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should compare SelectInput with snapshot', () => {
    expect(shallow(<SelectInput options={options} name="Test" isDirty handleChange={jest.fn()} />)).toMatchSnapshot();
  });

  it('simulates onSelectChange', () => {
    const event = {
      target: {
        value: '',
      },
    };
    instance.onSelectChange(event);
    expect(instance.state.value).toBe('');
  });
});
