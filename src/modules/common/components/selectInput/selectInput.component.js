import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './selectInput.scss';

/**
 * @description Custom select component
 */
class SelectInput extends PureComponent {
  /**
   * @description state is required here to clear select value after add repository to favourite
   * It also prevent unnecessary renders to other components not related with select
   */
  state = {
    value: '',
  };

  /**
   * @description set state from props when successfully added repository to favourite
   * @returns state
   * @param {Boolean} props.isDirty
   */
  static getDerivedStateFromProps(props) {
    if (!props.isDirty) {
      return {
        value: '',
      };
    }
    return null;
  }

  /**
   * @description handles select change and trigger callback to tell if value is selected
   */
  onSelectChange = (event) => {
    const { target: { value } } = event;
    const { handleChange, name } = this.props;
    this.setState({ value });
    handleChange(name, value);
  }

  /**
   * @description template for select component. By default empty value is set. After successfull
   * add favourite repository to favourites list, select is set to empty again.
   */
  render() {
    const { options } = this.props;
    const { value } = this.state;
    return (
      <form className="select-style">
        <select
          value={value}
          name="language"
          id="language"
          className={(value ? 'valid' : null)}
          onChange={this.onSelectChange}
          onBlur={this.onSelectChange}
        >
          <option value="" disabled />
          {Object.keys(options).map(optionKey => (
            <option value={optionKey} key={optionKey}>
              {options[optionKey]}
            </option>
          ))}
        </select>
        <label htmlFor="language">
          <span className="label-placeholder">Language</span>
        </label>
      </form>
    );
  }
}
/**
 * @description model of properties passed to SelectInput component
 */
SelectInput.propTypes = {
  options: PropTypes.objectOf(PropTypes.string).isRequired,
  name: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
};
export default SelectInput;
