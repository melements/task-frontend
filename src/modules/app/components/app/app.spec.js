import React from 'react';
import { shallow } from 'enzyme';
import App from './app.component';

describe('shoulds test App component', () => {
  const props = {
    isMobile: true,
    setMobile: jest.fn(),
  };
  it('should compare App with snapshot', () => {
    const wrapper = shallow(<App {...props} />);
    expect(wrapper).toMatchSnapshot();
  });
});
