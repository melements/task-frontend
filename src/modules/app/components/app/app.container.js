import { connect } from 'react-redux';

import { setMobile } from './app.actions';
import App from './app.component';

const mapDispatchToProps = {
  setMobile,
};

/**
 * @description passes methods to App Component
 */
export default connect(
  null,
  mapDispatchToProps,
)(App);
