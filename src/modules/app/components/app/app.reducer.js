import update from 'immutability-helper';

import {
  SET_MOBILE,
} from './app.actions';

/**
 * @description initial State for App Reducer
 */
const initialState = {
  isMobile: false,
};

/**
 * @description set state regarding to action
 */
export default (state = initialState, { type }) => {
  switch (type) {
    case SET_MOBILE: {
      return update(state, {
        isMobile: { $set: true },
      });
    }
    default:
      return state;
  }
};
