const namespace = 'APP';

export const SET_MOBILE = `${namespace}_SET_MOBILE`;

/**
 * @description action used to indicate if it is mobile device
 */
export const setMobile = () => ({
  type: SET_MOBILE,
});
