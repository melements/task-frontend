import React, { PureComponent, Fragment } from 'react';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import { MOBILE_BROWSER } from 'modules/app/config/config';

import NoMatch from 'modules/common/components/noMatch/noMatch.component';
import Navbar from 'modules/common/components/navbar/navbar.component';
import Favourites from 'modules/favourites/components/favourites/favourites.component';
import SearchResults from 'modules/searchResults/components/searchResults/searchResults.container';

/**
 * @description Root component.
 */
class App extends PureComponent {
  /**
   * @description checks if browser is on mobile device.
   * Will be treat as mobile if one of following mobile device is used
   * Mobile, Windows Phone, Lumia, Android, webOS, iPhone, iPod,
   * Blackberry, PlayBook, BB10, Opera Mini, bCrMo, Opera Mobi;
   */
  componentDidMount() {
    const { setMobile } = this.props;
    const isMobile = navigator.userAgent.match(MOBILE_BROWSER);
    if (isMobile) {
      setMobile();
    }
  }

  /**
   * @description Renders template based on url path.
   * If none of path are matched, NoMatch component will be displayed.
   */
  render() {
    return (
      <Fragment>
        <Router>
          <Navbar>
            <Switch>
              <Route path="/" component={Favourites} exact />
              <Route path="/search/:query/:language" component={SearchResults} />
              <Route component={NoMatch} />
            </Switch>
          </Navbar>
        </Router>
      </Fragment>
    );
  }
}

/**
 * @description model of properties passed to App component
 */
App.propTypes = {
  setMobile: PropTypes.func.isRequired,
};

export default App;
