import { getRouteTitle, ROUTES } from './config';

describe('should test config', () => {
  ROUTES.forEach((routeObject) => {
    it('should test getRouteTitle function with given pathnames', () => {
      expect(getRouteTitle(routeObject.route)).toEqual(routeObject.navbarText);
    });
  });
});
