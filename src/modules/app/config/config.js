/**
 * @description all configuartions and constants goes here
 */
const ITEM_NAME = 'favourites';
const BASE_URL = 'https://api.github.com';
const MAX_LENGTH = 50;
const SEARCH_RESULTS = 'Search results';
const SEARCH_FAVOURITES = 'Search favourites';
const NO_RESULTS = 'No results';
const ROUTES = [
  { route: '/search', navbarText: SEARCH_RESULTS },
  { route: '/', navbarText: SEARCH_FAVOURITES },
];
const MOBILE_BROWSER = /Mobile|Windows Phone|Lumia|Android|webOS|iPhone|iPod|Blackberry|PlayBook|BB10|Opera Mini|\bCrMo\/|Opera Mobi/i;
const languages = {
  javascript: 'JavaScript',
  java: 'Java',
  python: 'Python',
  ruby: 'Ruby',
  go: 'Go',
  bash: 'Shell',
  typescript: 'TypeScript',
  groovy: 'Groovy',
  scala: 'Scala',
  kotlin: 'Kotlin',
  php: 'PHP',
  cpp: 'C++',
  c: 'C',
};

/**
 * @description gets route title from url path
 * @returns text displayed in navbar
 * @param {String} pathname url pathname
 */
const getRouteTitle = (pathname) => {
  const { navbarText } = ROUTES.find(item => pathname.includes(item.route));
  return navbarText;
};

export {
  ITEM_NAME,
  BASE_URL,
  MAX_LENGTH,
  SEARCH_RESULTS,
  SEARCH_FAVOURITES,
  NO_RESULTS,
  ROUTES,
  MOBILE_BROWSER,
  languages,
  getRouteTitle,
};
