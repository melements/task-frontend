import { combineReducers } from 'redux';
import app from 'modules/app/components/app/app.reducer';
import favourites from 'modules/favourites/components/favouritiesList/favouritesList.reducer';
import searchResults from 'modules/searchResults/components/searchResults/searchResults.reducer';

/**
 * @description reducers are combined here
 */
export default combineReducers({ app, favourites, searchResults });
