import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Masonry from 'react-masonry-component';

import Result from 'modules/searchResults/components/result/result.component';
import LoadMore from 'modules/searchResults/components/loadMore/loadMore.component';
import './searchResultsList.scss';

/**
 * @description search results component
 */
class SearchResultsList extends PureComponent {
  /**
  * @param {Refference} props.masonry reference object to masonry node
  */
  constructor(props) {
    super(props);
    this.masonry = React.createRef();
  }

  /**
   * @description load more results
   */
  handleClick = () => {
    const { handleLoadMore } = this.props;
    handleLoadMore();
  };

  /**
   * @description required for masonry to refresh it after opening results details
   */
  refresh = () => {
    this.masonry.current.masonry.layout();
  }

  render() {
    const { resultList, showMore, isLoading } = this.props;

    const elements = resultList.map(result => (
      <Result cb={this.refresh} result={result} key={result.id} />
    ));

    return (
      <div className="search-results-list-component">
        <div>
          <div className="list-container mobile-view">
            {elements}
          </div>
          <div className="list-container desktop-view">
            <Masonry ref={this.masonry}>{elements}</Masonry>
          </div>
        </div>
        <div className="button-container">
          {!isLoading && showMore && (<LoadMore handleClick={this.handleClick} />)}
          {isLoading && <div className="loader" />}
        </div>
      </div>
    );
  }
}

/**
 * @description model of properties passed to SearchResultsList component
 */
SearchResultsList.propTypes = {
  handleLoadMore: PropTypes.func.isRequired,
  resultList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    description: PropTypes.string,
    stargazers_count: PropTypes.number,
    languages: PropTypes.arrayOf(PropTypes.shape()),
  })).isRequired,
  showMore: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default SearchResultsList;
