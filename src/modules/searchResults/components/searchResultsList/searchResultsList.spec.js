import React from 'react';
import { shallow } from 'enzyme';
// import resultList from 'mock/searchResults.json';
import SearchResultsList from './searchResultsList.component';

describe('shoulds test SearchResultsList component', () => {
  const resultList = [
    {
      id: 98752393,
      name: 'QFT',
      description: 'A repository for the Quest For Tetris',
      stargazers_count: 88,
      language: 'Python',
    },
  ];
  const wrapper = shallow(<SearchResultsList isLoading={false} showMore resultList={resultList} handleLoadMore={jest.fn()} />);
  const instance = wrapper.instance();

  it('should compare SearchResultsList with snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('simulates handleClick', () => {
    instance.handleClick();
  });
});
