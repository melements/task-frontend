import React from 'react';
import { shallow } from 'enzyme';
import LoadMore from './loadMore.component';

describe('shoulds test LoadMore component', () => {
  it('should compare LoadMore with snapshot', () => {
    const wrapper = shallow(<LoadMore handleClick={jest.fn()} />);
    expect(wrapper).toMatchSnapshot();
  });
});
