import React from 'react';
import PropTypes from 'prop-types';

/**
 * @description Simple button with callback
 * @param {Function} handleClick
 */
const LoadMore = ({ handleClick }) => (
  <button type="button" className="load-more-button" onClick={handleClick}>
    LOAD MORE
  </button>
);

/**
 * @description model of properties passed to LoadMore component
 */
LoadMore.propTypes = {
  handleClick: PropTypes.func.isRequired,
};

export default LoadMore;
