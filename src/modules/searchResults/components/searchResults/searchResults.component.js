import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { SEARCH_RESULTS, NO_RESULTS } from 'modules/app/config/config';
import DocumentTitle from 'react-document-title';
import SearchResultsList from 'modules/searchResults/components/searchResultsList/searchResultsList.component';

/**
 * @description Component to display results of search
 */
export class SearchResults extends PureComponent {
  /**
   * @description get repositories from storage
   * @async
   */
  async componentDidMount() {
    this.getRepos();
  }

  /**
   * @description clears search date on pop state
   */
  componentDidUpdate() {
    const { clearSearchData } = this.props;
    window.onpopstate = () => {
      clearSearchData();
    };
  }

  /**
   * @description load more results
   * @async
   */
  loadMore = async () => {
    const { incrementPage } = this.props;
    await incrementPage();
    this.getRepos();
  };

  /**
   * @description get repositories with passed params
   */
  getRepos = () => {
    const {
      page,
      pageSize,
      match: {
        params: { query, language },
      },
      getRepositories,
    } = this.props;
    getRepositories({
      name: query,
      language,
      page,
      pageSize,
    });
  };

  render() {
    const {
      isLoading,
      repoList,
      total,
      page,
      pageSize,
    } = this.props;

    const showMore = total - page * pageSize > 0;

    if (!isLoading && !repoList.length) {
      return (
        <DocumentTitle title={NO_RESULTS}>
          <h4 className="no-results">No results</h4>
        </DocumentTitle>
      );
    }

    return (
      <DocumentTitle title={SEARCH_RESULTS}>
        <div className="main-container">
          <SearchResultsList
            resultList={repoList}
            showMore={showMore}
            handleLoadMore={this.loadMore}
            isLoading={isLoading}
          />
        </div>
      </DocumentTitle>
    );
  }
}

/**
 * @description model of properties passed to SearchResults component
 */
SearchResults.propTypes = {
  clearSearchData: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      query: PropTypes.string,
      language: PropTypes.string,
    }),
  }).isRequired,
  isLoading: PropTypes.bool.isRequired,
  getRepositories: PropTypes.func.isRequired,
  repoList: PropTypes.arrayOf(PropTypes.shape({
    asd: PropTypes.string,
  })).isRequired,
  total: PropTypes.number.isRequired,
};

export default SearchResults;
