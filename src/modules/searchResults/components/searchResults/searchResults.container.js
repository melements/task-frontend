import { connect } from 'react-redux';

import SearchResultsComponent from './searchResults.component';
import {
  getRepositories,
  incrementPage,
  clearSearchData,
} from './searchResults.actions';

/**
 * @description map redux state to SearchResultsComponent's props
 */
const mapStateToProps = ({ searchResults }) => ({
  total: searchResults.total,
  pageSize: searchResults.pageSize,
  page: searchResults.page,
  repoList: searchResults.data,
  isLoading: searchResults.isLoading,
});

/**
 * @description map redux action to SearchResultsComponent's props
 */
const mapDispatchToProps = {
  getRepositories,
  incrementPage,
  clearSearchData,
};

/**
 * @description passes properties and methods to SearchResultsComponent component
 */
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchResultsComponent);
