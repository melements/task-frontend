import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import expect from 'expect';
import searchResults from 'mock/searchResults.json';

import {
  SET_RESULTS,
  CLEAR_RESULTS,
  SET_PAGE,
  getRepositories,
  clearSearchData,
  incrementPage,
  IS_LOADING,
} from './searchResults.actions';
import searchResultsReducer from './searchResults.reducer';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('test search repo actions', () => {
  const initialState = {
    isLoading: true,
    data: [],
    page: 1,
    pageSize: 20,
    total: 0,
  };

  const expectedResult = {
    data: expect.any(Array),
    isLoading: expect.any(Boolean),
    page: expect.any(Number),
    pageSize: expect.any(Number),
    total: expect.any(Number),
  };

  const expectedActionResult = [
    { payload: expect.any(Boolean), type: IS_LOADING },
    { payload: expect.any(Object), type: SET_RESULTS },
    { payload: expect.any(Boolean), type: IS_LOADING },
    { payload: expect.any(Boolean), type: IS_LOADING },
    { payload: expect.any(Array), type: CLEAR_RESULTS },
    { payload: expect.any(Number), type: SET_PAGE },
  ];

  const store = mockStore({
    searchResults: initialState,
  });

  it('reducer should return the initial state', () => {
    expect(searchResultsReducer(undefined, {})).toEqual(initialState);
  });

  it('reducer should handle IS_LOADING', () => {
    expect(
      searchResultsReducer(initialState, {
        type: IS_LOADING,
        payload: true,
      }),
    ).toEqual(initialState);
  });

  it('reducer should handle SET_RESULTS', () => {
    expect(
      searchResultsReducer(initialState, {
        type: SET_RESULTS,
        payload: searchResults,
      }),
    ).toEqual(expectedResult);
  });

  it('reducer should handle CLEAR_RESULTS', () => {
    initialState.data.push(searchResults.items);
    const result = Object.assign({}, expectedResult, { data: [] });
    expect(
      searchResultsReducer(initialState, {
        type: CLEAR_RESULTS,
        payload: [],
      }),
    ).toEqual(result);
  });

  it('reducer should handle SET_PAGE', () => {
    const page = 1;
    const result = Object.assign({}, expectedResult, { page });
    expect(
      searchResultsReducer(initialState, {
        type: SET_PAGE,
        payload: page,
      }),
    ).toEqual(result);
  });

  it('should create an action to get repositories', async () => {
    const params = { query: 'Tetris', language: 'python' };
    const expectedActions = [
      { payload: expect.any(Boolean), type: IS_LOADING },
      { payload: expect.any(Object), type: SET_RESULTS },
    ];

    fetch.mockResponseOnce(JSON.stringify(searchResults));

    await store.dispatch(getRepositories(params));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should FAIL create an action to get repositories', async () => {
    const params = { query: 'Tetris', language: 'python' };

    fetch.mockResponseOnce(JSON.stringify(searchResults), { status: 500 });
    try {
      await store.dispatch(getRepositories(params));
    } catch (error) {
      expect(error).toBeDefined();
    }
  });

  it('should create an action to clear search data', async () => {
    await store.dispatch(clearSearchData());
    expect(store.getActions()).toMatchObject(expectedActionResult);
  });

  it('should create an action to increment page', () => {
    const expectedActions = expectedActionResult;
    expectedActions.push({ payload: expect.any(Number), type: SET_PAGE });
    store.dispatch(incrementPage());
    expect(store.getActions()).toMatchObject(expectedActionResult);
  });
});
