import * as github from 'services/github';

const namespace = 'SEARCH_RESULTS';

const IS_LOADING = `${namespace}_IS_LOADING`;
const SET_RESULTS = `${namespace}_SET_RESULTS`;
const CLEAR_RESULTS = `${namespace}_CLEAR_RESULTS`;
const SET_PAGE = `${namespace}_SET_PAGE`;

/**
 * @description get repositories from github.api
 * @param {Object} query
 * @param {Object} language
 */
const getRepositories = (query, language) => async (dispatch) => {
  dispatch({
    type: IS_LOADING,
    payload: true,
  });

  try {
    const response = await github.getRepositories(query, language);
    dispatch({
      type: SET_RESULTS,
      payload: response,
    });
  } catch (err) {
    throw new Error(err);
  }
};

/**
 * @description clear search data
 */
const clearSearchData = () => (dispatch) => {
  dispatch({
    type: IS_LOADING,
    payload: true,
  });

  dispatch({
    type: CLEAR_RESULTS,
    payload: [],
  });

  dispatch({
    type: SET_PAGE,
    payload: 1,
  });
};

/**
 * @description increments page number
 */
const incrementPage = () => (dispatch, getState) => {
  const {
    searchResults: { page },
  } = getState();

  const nextPage = page + 1;

  dispatch({
    type: SET_PAGE,
    payload: nextPage,
  });
};

export {
  IS_LOADING,
  SET_RESULTS,
  SET_PAGE,
  CLEAR_RESULTS,
};

export {
  getRepositories,
  clearSearchData,
  incrementPage,
};
