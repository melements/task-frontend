import update from 'immutability-helper';

import {
 IS_LOADING, SET_RESULTS, SET_PAGE, CLEAR_RESULTS,
} from './searchResults.actions';

/**
 * @description initial state for searchResults
 */
const initialState = {
  isLoading: true,
  data: [],
  page: 1,
  pageSize: 20,
  total: 0,
};

/**
 * @description set state regarding to action
 */
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case IS_LOADING:
      return update(state, {
        isLoading: { $set: payload },
      });
    case SET_RESULTS: {
      return update(state, {
        data: { $push: [...payload.items] },
        total: { $set: payload.total_count },
        isLoading: { $set: false },
      });
    }
    case CLEAR_RESULTS: {
      return update(state, {
        data: { $set: payload },
      });
    }
    case SET_PAGE: {
      return update(state, {
        page: { $set: payload },
      });
    }
    default:
      return state;
  }
};
