import React from 'react';
import { shallow } from 'enzyme';
import SearchResults from './searchResults.component';

describe('shoulds test SearchResults component', () => {
  const match = {
    params: { query: 'react', language: 'javascript' },
  };
  const wrapper = shallow(<SearchResults
    page={1}
    pageSize={20}
    match={match}
    isLoading={false}
    getRepositories={jest.fn()}
    repoList={[{
      id: 98752397,
      name: 'QFT',
      description: 'A repository for the Quest For Tetris',
      stargazers_count: 88,
      language: 'Python',
    },
    {
      id: 76531881,
      name: 'tetris',
      description: ':space_invader: The original TETRIS game simulator',
      stargazers_count: 87,
      language: 'JavaScript',
    },
    {
      id: 9244997,
      name: 'Blockinger',
      description: 'Tetris clone for Android',
      stargazers_count: 86,
      watchers_count: 86,
      language: 'Java',
    }]}
    total={100}
    clearSearchData={jest.fn()}
  />);
  const instance = wrapper.instance();

  it('should compare SearchResults with snapshot', () => {
    instance.setState({});
    expect(wrapper).toMatchSnapshot();
  });

  it('should render no results', () => {
    const wrapperWithNoRepoList = shallow(<SearchResults
      page={1}
      pageSize={20}
      match={match}
      isLoading={false}
      getRepositories={jest.fn()}
      repoList={[]}
      total={100}
      clearSearchData={jest.fn()}
    />);
    expect(wrapperWithNoRepoList.find('.no-results').length).toEqual(1);
  });

  it('simulates loadMore', () => {
    instance.loadMore();
    expect(wrapper.find('.listItem').length).toBe(0);
  });
});
