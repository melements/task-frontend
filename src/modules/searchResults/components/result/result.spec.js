import React from 'react';
import { shallow } from 'enzyme';

import Result from './result.component';

describe('shoulds test Result component', () => {
  const props = {
    result: {},
    cb: jest.fn(),
  };
  const wrapper = shallow(<Result {...props} />);
  const instance = wrapper.instance();

  it('should compare Result with snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('simulates handleClick', () => {
    instance.handleClick();
  });
});
