import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _isEmpty from 'lodash/isEmpty';

import Details from 'modules/searchResults/components/details/details.component';
import { getLanguageList } from 'services/github';

import './result.scss';

/**
 * @description Result component to display repository card
 */
class Result extends PureComponent {
  /**
   * @description component state
   * @param {Boolean} isOpened indicates if details are opened
   * @param {Boolean} isLoading indicates if loader is displaed
   * @param {Object} languages languages for details
   */
  state = {
    isOpened: false,
    isLoading: false,
    languages: {},
  };

  /**
   * @description click handler. Gets languages list and pass it to Details component
   * @async
   */
  handleClick = async () => {
    const { isOpened, languages } = this.state;
    const { cb } = this.props;

    if (_isEmpty(languages)) {
      this.setState({ isLoading: true }, cb);
      const {
        result: {
          name,
          owner: { login },
        },
      } = this.props;
      const languagesList = await getLanguageList(login, name);
      this.setState({ isOpened: !isOpened, languages: languagesList, isLoading: false }, cb);
    } else {
      this.setState({ isOpened: !isOpened, isLoading: false }, cb);
    }
  };

  render() {
    const { isOpened, languages, isLoading } = this.state;
    const { result } = this.props;

    return (
      <div className="list-item bg-white shadow result-item">
        <div className="item-title">
          <span className="word-wrap-break">{`${result.name} - ${result.description}`}</span>
        </div>
        <span className="chip chip-yellow">
          {`${result.stargazers_count} Stars`}
        </span>
        <Details
          isOpened={isOpened}
          languages={languages}
          isLoading={isLoading}
          handleClick={this.handleClick}
        />
      </div>
    );
  }
}

/**
 * @description model of properties passed to Result component
 */
Result.propTypes = {
  result: PropTypes.shape({
    id: PropTypes.number,
    stargazers_count: PropTypes.number,
    name: PropTypes.string,
    description: PropTypes.string,
    languages_url: PropTypes.string,
  }).isRequired,
  cb: PropTypes.func.isRequired,
};

export default Result;
