import React from 'react';
import { shallow } from 'enzyme';
import { languages } from 'modules/app/config/config';
import Details from './details.component';

describe('shoulds test Details component', () => {
  it('should compare Details with snapshot', () => {
    const wrapper = shallow(<Details isLoading={false} isOpened={false} languages={{}} handleClick={jest.fn()} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should compare Details with snapshot when isLoading is true', () => {
    const wrapper = shallow(<Details isLoading isOpened={false} languages={{}} handleClick={jest.fn()} />);
    expect(wrapper.find('.loader').length).toEqual(1);
  });

  it('should compare Details with snapshot when isLoading is true and isOpened true', () => {
    const wrapper = shallow(<Details isLoading isOpened languages={languages} handleClick={jest.fn()} />);
    expect(wrapper.find('.chips-container').length).toEqual(1);
    expect(wrapper.find('.chip').length).toEqual(13);
  });
});
