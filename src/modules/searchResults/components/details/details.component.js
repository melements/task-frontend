import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import ChevronDown from 'assets/icons/chevronDown.component';
import ChevronUp from 'assets/icons/chevronUp.component';
import './details.scss';

/**
 * @description details component for Result Component. Displays languages used in repository
 */
class Details extends PureComponent {
  render() {
    const {
      isLoading,
      isOpened,
      languages,
      handleClick,
    } = this.props;

    return (
      <div className="details-component">
        {isLoading && <div className="loader loader-sm gray" />}
        {isOpened && (
          <div className="chips-container">
            {Object.keys(languages).map(lang => (
              <span key={lang} className="chip">
                {`${lang} ${languages[lang]}`}
              </span>
            ))}
          </div>
        )}
        <button
          className="chevron"
          aria-label="Expand languages"
          type="button"
          onClick={handleClick}
          onKeyPress={handleClick}
        >
          {isOpened ? <ChevronUp /> : <ChevronDown />}
        </button>
      </div>
    );
  }
}

/**
 * @description model of properties passed to Details component
 */
Details.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isOpened: PropTypes.bool.isRequired,
  languages: PropTypes.objectOf(PropTypes.string).isRequired,
  handleClick: PropTypes.func.isRequired,
};

export default Details;
