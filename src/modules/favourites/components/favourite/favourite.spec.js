import React from 'react';
import { shallow } from 'enzyme';
import Favourite from './favourite.component';

describe('shoulds test Favourite component', () => {
  const props = {
    history: {
      push: jest.fn(),
    },
    isMobile: true,
    favourite: {
      id: '5ee08a0c-cf65-4514-9b96-985a89019a6b',
      key: 'Tetris',
      value: 'python',
    },
    removeFavourite: jest.fn(),
  };
  const wrapper = shallow(<Favourite.WrappedComponent {...props} />);
  const instance = wrapper.instance();

  it('should compare Favourite with snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('simulate method ', () => {
    const spy = jest.spyOn(instance, 'handleDelete');
    instance.forceUpdate();
    wrapper.find('.button-delete').simulate('click', { stopPropagation: jest.fn(), target: { id: '5ee08a0c-cf65-4514-9b96-985a89019a6b' } });
    expect(spy).toHaveBeenCalled();
  });

  it('simulate item click', () => {
    const spy = jest.spyOn(instance, 'handleClick');
    instance.forceUpdate();
    wrapper.find('.favourite-item').simulate('click');
    expect(spy).toHaveBeenCalled();
  });
});
