import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Swipeout from 'rc-swipeout';
import 'rc-swipeout/assets/index.css'; // required for Swipeout
import SvgComponent from 'assets/icons/trash.component';
import './favourite.scss';

/**
 * @description favourite component displayed in favouriteList
 */
class Favourite extends PureComponent {
  /**
   * @description handle delete action on desktop
   * @param {Event} e
   */
  handleDelete = (e) => {
    e.stopPropagation();
    const { id } = e.target;
    const { removeFavourite } = this.props;
    removeFavourite(id);
  }

  /**
   * @description handle click event on favourite item.
   */
  handleClick = () => {
    const {
      history,
      favourite: { key, value },
    } = this.props;
    history.push({
      pathname: `/search/${key}/${value}`,
    });
  }

  /**
   * @description handle mobile delete action. Stop propagation to not open element.
   * @param {Event} e
   * @param {String} id
   */
  handleMobileDelete = (e, id) => {
    e.stopPropagation();
    const { removeFavourite } = this.props;
    removeFavourite(id);
  };

  render() {
    const { favourite: { key, value, id }, isMobile } = this.props;

    /**
     * @description simple configuration for swipeout component
     * @param {String} text text shown on swipeout
     * @param {Function} onPress callback function triggered on press
     * @param {String} className css classname to style Delete placeholder
     */
    const swipeoutConfig = [{
      text: 'DELETE',
      onPress: event => this.handleMobileDelete(event, id),
      className: 'swipeout-mask',
    }];

    /**
    * @description template for favourite item. If mobile device is used, swipeout is enabled.
    * Otherwise delete button will be displayed on hover.
    */
    return (
      <div
        onClick={this.handleClick}
        className={`favourite-item list-item shadow bg-white ${isMobile ? 'mobile' : null}`}
        role="button"
        tabIndex="0"
        onKeyPress={this.handleClick}
      >
        <Swipeout
          className="swipeout-element"
          right={swipeoutConfig}
          disabled={!isMobile}
        >
          <div className="swipeout-container">
            <div className="item-title">
              <span className="word-wrap-break">{key}</span>
            </div>
            <div className="bottom-container">
              <span className="chip">
                {value}
              </span>
              <button
                type="button"
                className="button-delete"
                onClick={this.handleDelete}
                id={id}
              >
                <SvgComponent />
                DELETE
              </button>
            </div>
          </div>
        </Swipeout>
      </div>
    );
  }
}

/**
 * @description model of properties passed to Favourite component
 */
Favourite.propTypes = {
  removeFavourite: PropTypes.func.isRequired,
  favourite: PropTypes.shape({
    key: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
  isMobile: PropTypes.bool.isRequired,
};

export default withRouter(Favourite);
