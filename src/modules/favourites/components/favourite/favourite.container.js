import { connect } from 'react-redux';

import { removeFavourite } from 'modules/favourites/components/favouritiesList/favouritesList.actions';
import Favourite from './favourite.component';

/**
 * @description map redux state to Favourite's props
 */
const mapStateToProps = ({ app }) => ({
  isMobile: app.isMobile,
});

/**
 * @description map redux action to Favourite's props
 */
const mapDispatchToProps = {
  removeFavourite,
};

/**
 * @description passes properties and methods to Favourite component
 */
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Favourite);
