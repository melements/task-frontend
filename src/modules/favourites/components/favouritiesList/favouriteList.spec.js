import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import expect from 'expect';

import {
  ADD_FAVOURITE,
  SET_FAVOURITES,
  REMOVE_FAVOURITE,
  setFavourite,
  getFavourites,
  removeFavourite,
  IS_LOADING,
} from './favouritesList.actions';
import favouritesListReducer from './favouritesList.reducer';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('test favourites actions', () => {
  const initialState = {
    isLoading: true,
    list: [],
  };

  const favourite = {
    id: '5ee08a0c-cf65-4514-9b96-985a89019a6b',
    key: 'Tetris',
    value: 'python',
  };

  const favourites = [
    { id: '5ee08a0c-cf65-4514-9b96-985a89019a6b', key: 'Tetris', value: 'kotlin' },
    { id: 'aaf0b7b9-aa39-4a18-92e5-709001f33878', key: 'Test 1', value: 'python' },
    { id: '21b42d05-6ea2-45a6-9fbe-9e369e0dd646', key: 'Test 2', value: 'typescript' },
    { id: '95306ce3-b090-488d-a67f-1d4f045b2d64', key: 'Test 3', value: 'ruby' },
  ];

  const store = mockStore({
    isLoading: true,
    favourites: {
      list: favourites,
    },
  });

  it('should create an action to get a favourites with empty local storage', () => {
    const expectedActions = { payload: [], type: SET_FAVOURITES };

    const result = store.dispatch(getFavourites());
    expect(result).toEqual(expectedActions);
  });

  it('reducer should return the initial state', () => {
    expect(favouritesListReducer(undefined, {})).toEqual(initialState);
  });

  it('reducer should handle IS_LOADING', () => {
    expect(
      favouritesListReducer(initialState, {
        type: IS_LOADING,
        payload: false,
      }),
    ).toEqual({ isLoading: false, list: expect.any(Array) });
  });

  it('reducer should handle SET_FAVOURITES', () => {
    expect(
      favouritesListReducer(initialState, {
        type: SET_FAVOURITES,
        payload: favourites,
      }),
    ).toEqual({
      isLoading: false,
      list: favourites,
    });
  });

  it('reducer should handle SET_FAVOURITES', () => {
    expect(
      favouritesListReducer(initialState, {
        type: SET_FAVOURITES,
        payload: favourites,
      }),
    ).toEqual({
      isLoading: false,
      list: favourites,
    });
  });

  it('reducer should handle ADD_FAVOURITE', () => {
    expect(
      favouritesListReducer(initialState, {
        type: ADD_FAVOURITE,
        payload: favourite,
      }),
    ).toEqual({
      isLoading: false,
      list: expect.arrayContaining([expect.objectContaining(favourite)]),
    });
  });

  it('reducer should handle REMOVE_FAVOURITE', () => {
    expect(
      favouritesListReducer(initialState, {
        type: REMOVE_FAVOURITE,
        payload: favourites,
      }),
    ).toEqual({
      isLoading: false,
      list: favourites,
    });
  });

  it('should create an action to add a favourite', () => {
    const expectedActions = {
      type: ADD_FAVOURITE,
      payload: {
        id: expect.any(String),
        key: expect.any(String),
        value: expect.any(String),
      },
    };

    const result = store.dispatch(setFavourite(favourite));
    expect(result).toEqual(expectedActions);
  });

  it('should create an action to remove a favourite', () => {
    const id = '5ee08a0c-cf65-4514-9b96-985a89019a6b';
    const expectedActions = {
      payload: expect.arrayContaining([
        expect.not.objectContaining({
          id,
        }),
      ]),
      type: REMOVE_FAVOURITE,
    };

    const result = store.dispatch(removeFavourite(id));
    expect(result).toEqual(expectedActions);
  });

  it('should create an action to get a favourites', () => {
    const expectedActions = {
      payload: expect.arrayContaining([expect.any(Object)]),
      type: SET_FAVOURITES,
    };

    const result = store.dispatch(getFavourites());
    expect(result).toEqual(expectedActions);
  });

  it('should create an action to get a favourites', () => {
    const expectedActions = {
      payload: expect.arrayContaining([expect.any(Object)]),
      type: SET_FAVOURITES,
    };

    const result = store.dispatch(getFavourites());
    expect(result).toEqual(expectedActions);
  });
});
