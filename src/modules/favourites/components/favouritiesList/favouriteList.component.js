import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Masonry from 'react-masonry-component';

import Favourite from 'modules/favourites/components/favourite/favourite.container';

/**
 * @description component to display favourites repositories
 */
class FavouriteList extends PureComponent {
  /**
   * @description get Favourites repositories from storage
   */
  componentDidMount() {
    const { getFavourites } = this.props;
    getFavourites();
  }

  render() {
    const { list, isLoading } = this.props;

    /**
     * @description Takes favourites and converts it to list of react components
     * @returns list of elements
     */
    const favourites = list.map(favourite => (
      <Favourite favourite={favourite} key={favourite.id} />
    ));

    return !isLoading && (
      <div>
        <div className="list-container mobile-view">
          {favourites}
        </div>
        <div className="list-container desktop-view">
          <Masonry>{favourites}</Masonry>
        </div>
      </div>
    );
  }
}

/**
 * @description model of properties passed to FavouriteList component
 */
FavouriteList.propTypes = {
  getFavourites: PropTypes.func.isRequired,
  list: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    favourite: PropTypes.shape({}),
  })).isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default FavouriteList;
