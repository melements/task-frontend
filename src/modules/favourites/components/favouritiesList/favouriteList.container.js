import { connect } from 'react-redux';

import FavouriteList from './favouriteList.component';
import { getFavourites } from './favouritesList.actions';


/**
 * @description map redux state to FavouriteList's props
 */
const mapStateToProps = ({ favourites }) => ({
  list: favourites.list,
  isLoading: favourites.isLoading,
});

/**
 * @description map redux action to FavouriteList's props
 */
const mapDispatchToProps = {
  getFavourites,
};

/**
 * @description passes properties and methods to FavouriteList component
 */
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FavouriteList);
