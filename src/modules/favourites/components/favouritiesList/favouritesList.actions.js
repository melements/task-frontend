import { ITEM_NAME } from 'modules/app/config/config';
import * as storage from 'services/storage';

const namespace = 'FAVOURITES_LIST';

const IS_LOADING = `${namespace}_IS_LOADING`;
const SET_FAVOURITES = `${namespace}_SET_FAVOURITES`;
const ADD_FAVOURITE = `${namespace}_ADD_FAVOURITE`;
const REMOVE_FAVOURITE = `${namespace}_REMOVE_FAVOURITE`;

/**
 * @description get favourites elements from storage
 */
const getFavourites = () => (dispatch) => {
  const favourites = storage.getValue(ITEM_NAME);

  return dispatch({
    type: SET_FAVOURITES,
    payload: favourites || [],
  });
};

/**
 * @description set favourite repository to favourites list
 * @param {Object} favourite
 */
const setFavourite = favourite => (dispatch, getState) => {
  const {
    favourites: { list },
  } = getState();

  const newList = list.concat(favourite);
  storage.setValue(ITEM_NAME, newList);

  return dispatch({
    type: ADD_FAVOURITE,
    payload: favourite,
  });
};

/**
 * @description remove favourite repository from favourites list
 * @param {String} id
 */
const removeFavourite = id => (dispatch) => {
  const favourites = storage.getValue(ITEM_NAME);
  const filtered = favourites.filter(favourite => favourite.id !== id);

  storage.setValue(ITEM_NAME, filtered);

  return dispatch({
    type: REMOVE_FAVOURITE,
    payload: filtered,
  });
};

export {
  IS_LOADING,
  SET_FAVOURITES,
  ADD_FAVOURITE,
  REMOVE_FAVOURITE,
};

export {
  getFavourites,
  setFavourite,
  removeFavourite,
};
