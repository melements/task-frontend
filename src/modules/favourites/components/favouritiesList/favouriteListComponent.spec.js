import React from 'react';
import { shallow } from 'enzyme';
import FavouriteList from './favouriteList.component';

describe('shoulds test FavouriteList component', () => {
  it('should compare FavouriteList with snapshot', () => {
    const wrapper = shallow(<FavouriteList isLoading={false} list={[]} getFavourites={jest.fn()} />);
    expect(wrapper).toMatchSnapshot();
  });
});
