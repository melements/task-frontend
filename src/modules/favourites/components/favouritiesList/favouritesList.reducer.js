import update from 'immutability-helper';

import {
  IS_LOADING,
  SET_FAVOURITES,
  ADD_FAVOURITE,
  REMOVE_FAVOURITE,
} from './favouritesList.actions';

/**
 * @description initial state for favourites
 */
const initialState = {
  isLoading: true,
  list: [],
};

/**
 * @description set state regarding to action
 */
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case IS_LOADING: {
      return {
        ...state,
        isLoading: payload,
      };
    }
    case SET_FAVOURITES: {
      return update(state, {
        list: { $set: payload },
        isLoading: { $set: false },
      });
    }
    case ADD_FAVOURITE: {
      return update(state, {
        list: { $push: [payload] },
        isLoading: { $set: false },
      });
    }
    case REMOVE_FAVOURITE: {
      return update(state, {
        list: { $set: payload },
        isLoading: { $set: false },
      });
    }
    default:
      return state;
  }
};
