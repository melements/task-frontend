import React, { PureComponent } from 'react';
import DocumentTitle from 'react-document-title';

import { SEARCH_FAVOURITES } from 'modules/app/config/config';
import SearchForm from 'modules/favourites/components/searchForm/searchForm.container';
import FavouriteList from 'modules/favourites/components/favouritiesList/favouriteList.container';

/**
 * @description Presentational component to display SearchForm and FavouriteList
 * sets document title
 */
class Favourites extends PureComponent {
  render() {
    return (
      <DocumentTitle title={SEARCH_FAVOURITES}>
        <div className="main-container">
          <SearchForm />
          <FavouriteList />
        </div>
      </DocumentTitle>
    );
  }
}

export default Favourites;
