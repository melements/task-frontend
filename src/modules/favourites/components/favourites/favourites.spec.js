import React from 'react';
import { shallow } from 'enzyme';
import Favourites from './favourites.component';

describe('shoulds test Favourites component', () => {
  const wrapper = shallow(<Favourites />);

  it('should compare Favourites with snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
