import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';

import TextInput from 'modules/common/components/textInput/textInput.component';
import SelectInput from 'modules/common/components/selectInput/selectInput.component';
import { languages, MAX_LENGTH } from 'modules/app/config/config';
import AddIcon from 'assets/icons/add.component';
import './searchForm.scss';

class SearchRepo extends PureComponent {
  /**
   * @description this state is used to only hold information if form is valid and
   * input or select are not pristine. It will be reset on addToList
   */
  state = {
    isFormValid: false,
    isDirty: false,
  };

  /**
   * @description it holds input and select value and not cause to unnecessary render on
   * children components
   */
  values = {};

  /**
   * @description callback function triggered on input or select value change.
   * It also validates if form can be submited
   * @param {String} name
   * @param {String} value
   */
  handleChange = (name, value) => {
    this.values[name] = value;
    this.validateForm();
  }

  /**
   * @description validates if form is valid. It also sets isDirty flag to true
   */
  validateForm = () => {
    this.setState({
      isFormValid: this.values.repositoryName && this.values.language,
      isDirty: true,
    });
  };

  /**
   * @description after add favourite to list. It clear form and reset state
   */
  clearForm = () => {
    this.values.repositoryName = '';
    this.values.language = '';
    this.setState({ isDirty: false, isFormValid: false });
  }

  /**
   * @description It generates favourite with unique id and adds to list and trigger clearForm
   */
  addToList = () => {
    const { setFavourite } = this.props;
    const newFavourite = Object.assign(
      {},
      { id: uuid(), key: this.values.repositoryName, value: this.values.language },
    );
    setFavourite(newFavourite);
    this.clearForm();
  };

  /**
   * @description template for searchForm
   */
  render() {
    const { isFormValid, isDirty } = this.state;

    return (
      <div className="search-form bg-white shadow">
        <div className="inputs">
          <div className="input-item">
            <TextInput
              isDirty={isDirty}
              name="repositoryName"
              label="Repo name or description"
              handleChange={this.handleChange}
              maxLength={MAX_LENGTH}
            />
          </div>
          <div className="input-item">
            <SelectInput
              isDirty={isDirty}
              name="language"
              options={languages}
              handleChange={this.handleChange}
            />
          </div>
        </div>
        <div className="button-container">
          <button
            type="button"
            name="add-to-list"
            className="button"
            onClick={this.addToList}
            disabled={!isFormValid}
          >
            <AddIcon />
            ADD TO LIST
          </button>
        </div>
      </div>
    );
  }
}

/**
 * @description model of properties passed to SearchRepo component
 */
SearchRepo.propTypes = {
  setFavourite: PropTypes.func.isRequired,
};

export default SearchRepo;
