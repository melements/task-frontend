import React from 'react';
import { shallow } from 'enzyme';
import SearchRepo from 'modules/favourites/components/searchForm/searchForm.component';

describe('shoulds test search repo component', () => {
  const wrapper = shallow(<SearchRepo setFavourite={jest.fn()} />);
  const instance = wrapper.instance();

  it('should compare search repo with snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('simulates handleChange and change if values in component changed', () => {
    instance.handleChange('repositoryName', 'test');
    expect(instance.values).toEqual({ repositoryName: 'test' });
  });

  it('simulates addToList and clearForm methods', () => {
    instance.addToList();
    expect(instance.values.repositoryName).toEqual('');
  });
});
