import { connect } from 'react-redux';

import { setFavourite } from 'modules/favourites/components/favouritiesList/favouritesList.actions';
import SearchForm from './searchForm.component';

const mapStateToProps = () => ({});

/**
 * @description map redux actions to SearchForm's props
 */
const mapDispatchToProps = {
  setFavourite,
};

/**
 * @description passes methods to SearchForm component
 */
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchForm);
